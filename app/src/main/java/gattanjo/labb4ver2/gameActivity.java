package gattanjo.labb4ver2;

import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import java.util.ArrayList;

public class gameActivity extends AppCompatActivity {


    private Hangman hangman;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        String[] words = {"Horse","Vanilla","Madagascar","House","Maniac","Frankfurt","Keyboard","Mosaic"};

        String word = words[Hangman.randomNumber(words.length)];

        boolean[] visible = new boolean[word.length()];
        for (int i = 0; i < word.length(); i++) {
            visible[i] = false;
        }


        hangman = new Hangman(word , 10, visible, word.length());


        TextView tv = (TextView) findViewById(R.id.word);
        tv.setText(hangman.getHiddenWord());

    }

    public void Guess(View view) {
        EditText letter = (EditText) findViewById(R.id.letterInput);
        String str = letter.getText().toString();
        char Character = str.charAt(0);
        Log.d("Debug", "onClickWorks");
        hangman.Input(Character);
    }
}
