package gattanjo.labb4ver2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void goInfo(View view) {
        Intent intent = new Intent(this, infoActivity.class);
        startActivity(intent);
    }

    public void goGame(View view) {
        Intent intent2 = new Intent(this, gameActivity.class);
        startActivity(intent2);
    }
}
