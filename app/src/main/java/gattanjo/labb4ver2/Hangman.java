package gattanjo.labb4ver2;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by iths on 2015-10-25.
 */
public class Hangman {

    private String currentWord;
    private char[] hiddenWord;
    private int triesLeft;
    private boolean[] visible;
    private int wordSize;

    private ArrayList<Character> correctLetters = new ArrayList<Character>();
    private ArrayList<Character> incorrectLetters = new ArrayList<Character>();
    private ArrayList<Character> usedLetters = new ArrayList<Character>();

    public Hangman(String currentWord, int triesLeft, boolean[] visible, int wordSize) {
        setCurrentWord(currentWord);
        setTriesLeft(triesLeft);
        setVisible(visible);
        setWordSize(wordSize);
        StringToChar();

    }

    public void StringToChar () {
        hiddenWord = new char[currentWord.length()];
        for (int i = 0; i < currentWord.length(); i++) {
            hiddenWord[i] = '-';
        }
    }


    public static int randomNumber(int max) {

        Random rdm = new Random();
        int randomNum = rdm.nextInt((max - 0) + 1) + 0;
        return  randomNum;

    }


    public String getCurrentWord() {
        return currentWord;
    }

    public void setCurrentWord(String currentWord) {
        this.currentWord = currentWord;
    }

    public int getTriesLeft() {
        return triesLeft;
    }

    public void setTriesLeft(int triesLeft) {
        this.triesLeft = triesLeft;
    }

    public boolean[] getVisible() {
        return visible;
    }

    public void setVisible(boolean[] visible) {
        this.visible = visible;
    }

    public int getWordSize() {
        return wordSize;
    }

    public void setWordSize(int wordSize) {
        this.wordSize = wordSize;
    }

    public ArrayList<Character> getCorrectLetters() {
        return correctLetters;
    }

    public void setCorrectLetters(ArrayList<Character> correctLetters) {
        this.correctLetters = correctLetters;
    }

    public ArrayList<Character> getIncorrectLetters() {
        return incorrectLetters;
    }

    public void setIncorrectLetters(ArrayList<Character> incorrectLetters) {
        this.incorrectLetters = incorrectLetters;
    }

    public void setHiddenWord(char[] hiddenWord) {
        this.hiddenWord = hiddenWord;
    }

    public String getHiddenWord() {

        for (int j = 0; j < hiddenWord.length; j++) {
            for (int l = 0; l < correctLetters.size();l++) {
                if (hiddenWord[j] != correctLetters.get(l)) {
                    hiddenWord[j] = '-';
                }
            }
        }

        String a = "";
        for(int i = 0; i < currentWord.length();i++){
            a = a + hiddenWord[i];
        }

        return a;
    }

    public void Input(char x) {
        for (int j = 0; j < usedLetters.size(); j++) {
            if (x == correctLetters.get(j)) {
                return;
            }
        }

        for(int i = 0; i < currentWord.length(); i++) {
            if(x == hiddenWord[i]) {
                correctLetters.add(x);
                usedLetters.add(x);
                Log.d("Debug", "");
            }
            else {
                incorrectLetters.add(x);
                usedLetters.add(x);
            }
        }
    }
}